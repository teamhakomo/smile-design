(function() {
    'use strict';

    var $hamburgerToggle = $('.hamburger-toggle');
    var $navigation = $('.navigation');
    var $languageSelector = $('.language-selector');
    var $navList = $('.navigation > .navigation-list');
    var $hamburgerToggleText = $('.hamburger-toggle-text');
    var $navCategories = $('.navigation-list-item-text');
    var $navigationOverlay = $('.navigation-open-overlay');

    // This is needed for parallax.js
    jQuery(window).trigger('resize').trigger('scroll');
    $('.parallax-window').parallax({
        iosFix: false,
        androidFix: false
    });

    // close navigation when is opened and click otside ot it
    $('body').on('click touchstart', function(e){
        if ($(e.target).is('.navigation-open-overlay')) {
            e.stopPropagation();
            $hamburgerToggle.trigger( "click" );
        }
    });

    // $('html').removeClass('no-js');

    // Hamburger toggle. Toggles the open class of its parent
    $hamburgerToggle.click(function() {
        $hamburgerToggle.toggleClass('active');
        $hamburgerToggleText.toggleClass('active');
        $navigation.toggleClass('open');

        setTimeout(function(){
            if ($navigation.hasClass('open')) {
                $navigation.addClass('opened');
            }else{
                $navigation.removeClass('opened');
            }
        }, 250);

        if ($navigation.hasClass('open')) {
            $navList.show();
            $navigationOverlay.fadeIn('400');
        }else{
            $navList.hide();
            $navigationOverlay.fadeOut('400');
        }

        var newTxt = $hamburgerToggleText.hasClass('active') ?
            $hamburgerToggleText.data('activeTxt') :
            $hamburgerToggleText.data('txt');

        $hamburgerToggleText.text(newTxt);
    });

    // Toggle categories
    $navCategories.click(function(e){
        e.preventDefault();
        var $this = $(this);
        // check if other category is open and close all before open the new one

        $navCategories.next('.navigation-list').each(function(){
            var $that = $(this);

            if ($this != $that) {
                $that.slideUp('fast', function(){

                });
            }
        });

            var $thisNavList = $this.next('.navigation-list');
            if ($thisNavList.is(':visible')) {
                $this.next('.navigation-list').slideUp();
            }else{
                $this.next('.navigation-list').slideDown();
            }
    });


    // Language selector
    $languageSelector
    .find('.picker-language')
    .click(function() {
        var $btn = $(this);
        var lang = $btn.data('language');
        var $form = $languageSelector.find('.language-selector-form');

        $btn.toggleClass('active');
        $btn.siblings().toggleClass('active');

        $form.find('select[name=language]').val(lang);
        $form.find('input[name=next]').val($btn.data('url'));
        $form.submit();
    });

    // Animate on scroll reveal
    var seqInterval = 200;
    window.sr = ScrollReveal({
        duration: 1000,
        mobile: true,
        viewFactor: 0.3,
        scale: 1,
        delay: 50,
        useDelay: 'always',
        distance: '100px'
    });
    sr.reveal('.slide-right', { origin: 'right'}, seqInterval);
    sr.reveal('.slide-right-contact', { origin: 'right'}, seqInterval);

    sr.reveal('.slide-left', { origin: 'left'}, seqInterval);
    sr.reveal('.parallax-mirror', { origin: 'left'}, seqInterval);

    sr.reveal('.slide-top', { origin: 'top'}, seqInterval);

    sr.reveal('.slide-bottom-1', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-2', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-3', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-4', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-5', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-6', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-7', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-8', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-9', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-10', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-11', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-12', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-13', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-14', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-15', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-16', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-17', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-18', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-19', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-20', { origin: 'bottom'}, seqInterval);
    sr.reveal('.slide-bottom-footer', { origin: 'bottom'}, seqInterval);

    function svgTrace (domEl) {
        var $svg = $(domEl).find('.svg-trace-before');
        $svg.show('fast', function(){
            $svg.addClass('svg-trace-animation');
        });
    }

    function svgTraceHide (domEl) {
        var $svg = $(domEl).find('.svg-trace-before');
        $svg.hide('fast', function(){
            $svg.removeClass('svg-trace-animation');
        });
    }

    sr.reveal('.slide-home-logo-trace', {
        beforeReveal:svgTraceHide,
        origin: 'bottom',
        reset: true,
        afterReveal: svgTrace
    });

    sr.reveal('.slide-home-logo-trace-shadow', {
        beforeReveal:svgTraceHide,
        origin: 'bottom',
        reset: true,
        afterReveal: svgTrace
    });

    // Remove hover on mobile devices
    var $buttonsAndAnchors = $('a, button');

    $buttonsAndAnchors.on('touchstart mouseenter', function(){
        var $this = $(this);
        $this.addClass('hover');
    });

    $buttonsAndAnchors.on('mouseleave touchmove click', function(){
        var $this = $(this);
        $this.removeClass('hover');
    });

}());






