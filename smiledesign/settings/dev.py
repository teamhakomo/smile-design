from __future__ import absolute_import, unicode_literals

from .base import *

# To test production environment, set to False and uncomment LOGGING to see error logs
DEBUG = True

# Needed when DEBUG is False
ALLOWED_HOSTS = ['*']

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'tpm0jja%aq)2e67du@tl*7tff25@@9%_28q-5gwrhp8xx&yyl)'

# Uncomment when DEBUG is False
# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'handlers': {
#         # Include the default Django email handler for errors
#         # This is what you'd get without configuring logging at all.
#         'mail_admins': {
#             'class': 'django.utils.log.AdminEmailHandler',
#             'level': 'ERROR',
#              # But the emails are plain text by default - HTML is nicer
#             'include_html': True,
#         },
#         'console': {
#             'class': 'logging.StreamHandler'
#         }
#     },
#     'loggers': {
#         # Again, default Django configuration to email unhandled exceptions
#         'django.request': {
#             'handlers': ['mail_admins'],
#             'level': 'ERROR',
#             'propagate': True,
#         },
#         # Might as well log any errors anywhere else in Django
#         'django': {
#             'handlers': ['console'],
#             'level': 'ERROR',
#             'propagate': False,
#         },
#         # Your own app - this assumes all your logger names start with "smiledesign."
#         'smiledesign': {
#             'handlers': ['console'],
#             'level': 'WARNING', # Or maybe INFO or DEBUG
#             'propagate': False
#         },
#     },
# }

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

AKISMET_SITE_URL = 'localhost:8000'

AKISMET_TEST_MODE = True

try:
    from .local import *
except ImportError:
    pass
