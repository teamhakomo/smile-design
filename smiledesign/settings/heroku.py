from __future__ import absolute_import, unicode_literals

from .base import *
from smiledesign.S3 import CallingFormat

DEBUG = False

SECRET_KEY = '3jmma*f_&kjtuk-3h2z=y%yp)4q)3)t8q0p4ighk3$j@$g4a=h'

AKISMET_SITE_URL = os.environ.get('SMILEDESIGN_SITE_URL', 'https://smiledesign.herokuapp.com/')

AKISMET_TEST_MODE = False


##########
# HEROKU #
##########
# Parse database configuration from $DATABASE_URL
if 'DATABASE_URL' in os.environ:
    import dj_database_url
    DATABASES['default'] = dj_database_url.config()

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']


DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'


AWS_S3_REGION_NAME = os.environ.get('SMILEDESIGN_AWS_REGION', 'eu-west-2')
AWS_S3_SIGNATURE_VERSION = os.environ.get('SMILEDESIGN_AWS_SIGNATURE', 's3v4')
AWS_ACCESS_KEY_ID = os.environ.get('SMILEDESIGN_AWS_KEY', '')
AWS_SECRET_ACCESS_KEY = os.environ.get('SMILEDESIGN_AWS_SECRET', '')
AWS_STORAGE_BUCKET_NAME = os.environ.get('SMILEDESIGN_S3_BUCKET', 'smiledesign')
AWS_CALLING_FORMAT = CallingFormat.SUBDOMAIN
AWS_QUERYSTRING_AUTH = False
AWS_PRELOAD_METADATA = True

AWS_HEADERS = {  # see http://developer.yahoo.com/performance/rules.html#expires
    'Expires': 'Thu, 31 Dec 2099 20:00:00 GMT',
    'Cache-Control': 'max-age=94608000',
}


#########
# PATHS #
#########

DEFAULT_S3_PATH = 'media'

AWS_STORAGE_URL = 'https://{bucket}.s3.amazonaws.com/'.format(bucket=AWS_STORAGE_BUCKET_NAME)

MEDIA_ROOT = ''
MEDIA_URL = AWS_STORAGE_URL

if os.environ.get('ENABLE_CONSOLE_LOGSTREAM', False):
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
            },
        },
        'loggers': {
            'django': {
                'handlers': ['console'],
                'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
            },
        },
    }

try:
    from .local import *
except ImportError:
    pass
