from storages.backends.s3boto3 import S3Boto3Storage


class StaticS3Backend(S3Boto3Storage):
    location = 'static'
