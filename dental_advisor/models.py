from django.db import models
from django import forms

from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.tags import ClusterTaggableManager
from taggit.models import TaggedItemBase

from wagtail.wagtailcore.models import Page, Orderable
from wagtail.wagtailcore.fields import RichTextField
from wagtail.wagtailadmin.edit_handlers import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.wagtailimages.edit_handlers import ImageChooserPanel
from wagtail.wagtailsearch import index
from wagtail.wagtailsnippets.models import register_snippet
from django.utils.translation import ugettext_lazy as _
# Create your models here.
from services import TranslatedField


class DentalAdvisorPage(Page):
    title_bg = models.CharField(
        max_length=255,
        blank=True,
        help_text=_('The page title as you\'d like it to be seen by the public'),
        verbose_name='title [bg]'
    )
    intro = RichTextField(blank=True)
    subpage_types = ['DentalAdvisorArticle']

    translated_title = TranslatedField(
        'title',
        'title_bg'
    )
    content_panels = Page.content_panels + [
        FieldPanel('title_bg', classname='full title'),
    ]

    def get_context(self, request):
        # Update context to include only published posts, ordered by reverse-chron
        context = super(DentalAdvisorPage, self).get_context(request)
        articles = self.get_children().live().order_by('-first_published_at')
        context['articles'] = articles
        return context


class DentalAdvisorArticle(Page):
    title_bg = models.CharField(
        max_length=255,
        blank=True,
        help_text=_('The page title as you\'d like it to be seen by the public'),
        verbose_name='title [bg]'
    )
    translated_title = TranslatedField(
        'title',
        'title_bg'
    )
    date = models.DateField('Post date')

    description = TranslatedField(
        'description_en',
        'description_bg'
    )

    description_en = RichTextField(blank=True, verbose_name='description [en]')
    description_bg = RichTextField(blank=True, verbose_name='description [bg]')

    body = TranslatedField(
        'body_en',
        'body_bg'
    )

    body_en = RichTextField(blank=True, verbose_name='body [en]')
    body_bg = RichTextField(blank=True, verbose_name='body [bg]')

    featured_image = models.ForeignKey(
        'wagtailimages.Image',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        related_name='+'
    )

    search_fields = Page.search_fields + [
        index.SearchField('title_bg'),
        index.SearchField('body_en'),
        index.SearchField('body_bg')
    ]

    content_panels = Page.content_panels + [
        FieldPanel('title_bg', classname='full title'),
        ImageChooserPanel('featured_image'),
        MultiFieldPanel([
            FieldPanel('date'),
        ], heading='Article information'),
        FieldPanel('description_en'),
        FieldPanel('description_bg'),
        FieldPanel('body_en', classname='full title'),
        FieldPanel('body_bg', classname='full title'),
    ]

    parent_page_types = ['DentalAdvisorPage']
    subpage_types = []


class ArticleGalleryImage(Orderable):
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.CASCADE, related_name='+'
    )
    caption = models.CharField(blank=True, max_length=250)

    panels = [
        ImageChooserPanel('image'),
        FieldPanel('caption'),
    ]
