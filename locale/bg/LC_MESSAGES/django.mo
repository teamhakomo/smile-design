��    r      �  �   <      �	  R   �	  T   
  R   Y
  )   �
  b   �
  W   9  Z   �  X   �  Z   E  �   �  �   1  _   �      �  (  �  �  �  z  T  O  H   �  <   �  �   *  V  �  �    *   �  �  �  C   �  E   �  �  4  �   $!    �!  A   �"  f   '#  D   �#  5   �#  :   	$  �  D$    �%  �  �&  �   ~)  .   *  A   2*  J   t*  ;   �*  �  �*  �  �,  i   G.     �.  �   �.     j/     r/  V   x/  5   �/     0  1   0  	   >0     H0  =   N0     �0     �0  
   �0     �0  	   �0     �0  	   �0     �0  	   �0     �0     �0  ?   �0     *1     :1  N   J1  G   �1     �1     �1  P   �1     I2  ^   [2     �2  �   �2     l3     ~3     �3     �3  1  �3     �4    �4     �5  	   6     6     6  �   )6  1   7     H7  +   O7     {7     �7  	   �7     �7    �7     �8     �8  
   �8  $   �8     �8     �8  
   9     9     !9     )9  ;   E9     �9     �9     �9  r  �9  B   ;  J   R;  G   �;     �;  >   �;  9   :<  ?   t<  9   �<  ?   �<  �   .=  B   �=  6   >  *  J>  �  u@  )  qD  (  �G  �  �L  @   TO  ?   �O     �O  @  �O  �  V     �X  -  Y  J   ?]  Q   �]  �  �]    �a  �  �b  6   �d  �   �d  P   ge  I   �e  E   f  �  Hf  �  �i  �  �k    *q  (   :r  T   cr  W   �r  C   s  �  Ts  �
  Gv  ~   �  1   m�  m   ��  
   �     �  �   !�  a   ��  
   �  H   "�     k�     ��  �   ��     �     )�     8�     N�     _�     o�     ~�     ��     ��  
   ��     ��  �   ˄     ��     ��  �   ą  v   o�     �     ��  �   �     ��  �   Ӈ     ��  �   ��     r�  +   ��     ��     ډ  �  �     ��  �  ҋ     Z�  
   i�     t�     }�  "  ��  ]   ��  
   �  M   )�     w�     ��     ��  #   ��  W  ؒ     0�     ?�     L�  z   \�  $   ו     ��     �  @   "�     c�  N   p�  V   ��  	   �      �  
   2�     g   :   l   2       !   ]                     '       +   5      -       E              0   i          K   _      X   k   &   q      S   B   %   J   a   ^                           3       G   L      Y      T   o      b              r   .   e   6   M   A   1   h   @   #   N   =   Z       "   V       c   Q       C   I                 f   >      j           )   P       <       \       8               
              H   d       `   $          m                 	   7                  ,   F   *   n           W   ?      9   (   ;   O   p                     D      4   /   U              [   R    
                            <span class="text-extra-light">Dental</span> Implants 
                            <span class="text-extra-light">Porcelain</span> Veneers 
                            <span class="text-extra-light">Teeth</span> Whitening 
                            Orthodontics 
                        <span class="text-extra-light">Learn</span> More
                         
                    <span class="text-thin">Dr. Dano</span> Danov
                     
                    <span class="text-thin">Dr. Jenny</span> Kadreva
                     
                    <span class="text-thin">Dr. MAGI</span> NASHAR
                     
                    <span class="text-thin">Dr. Selar</span> Francis
                     
                    <span class="text-thin">Teeth</span><br>
                    <span class="text-light">Whitening</span>
                     
                    Dental
                    <br>
                    <span class="text-normal">Services</span>
                     
                    WE PROVIDE A WIDE RANGE OF DENTAL SERVICES INCLUDING:
                     
                A beautiful smile! Teeth whitening can erase stains and discolourations caused by food, beverages, tobacco, medications, and age. Our uses the latest in bleaching technology to safely and effectively deliver beautiful, brilliant smiles.
                 
                A dental implant is a titanium post that mimics a tooth root. One implant is placed in your jaw for each missing tooth, or a few to secure a denture, partial, or bridge. Your jaw-bone integrates with the titanium to create a strong foundation for a dental crown or prosthetic. Dental implants restore dental structure to make faces appear fuller and more youthful. They also prevent bone deterioration caused by missing teeth.
                 
                Absolutely! Veneers are made of fine dental porcelain that reflects light like natural tooth enamel, so they look and feel completely natural. In fact, porcelain veneers are virtually undetectable – no one will know you have them, but they will notice your stunning smile! Their resemblance to natural teeth is unsurpassed by other restorative options.
                 
                Adequate jawbone structure is required for implants. Dr. Francis will perform a comprehensive examination and use the <span class="text-semi-bold">latest digital technology</span>, utilising the <span class="text-semi-bold">Cone Beam Computerized Tomography (CBCT)</span> which allows us to see your jaw bones three dimensionally. It will provide a guide for the safe placement of your implants.
He’ll also evaluate your health history to assess your candidacy. If you qualify for dental implants, Dr. Francis will explain the implant procedure and answer your questions. Your concerns are important to us, and our entire team will work closely with you to make your experience a success.
                 
                Almost anyone with stained or discoloured teeth can achieve a brighter smile with professional bleaching. Please note, though, that treatment may take longer time to be as effective for some patients as it is for others. We will examine your teeth to determine if whitening is a good choice for your smile.
                 
                Am I a candidate for teeth whitening? 
                 
                Are implants right for me?
                 
                As an experienced implantologist, Dr. Francis restores smiles and lives with custom dental implants.
                 
                At this appointment Dr. Francis will prepare your teeth for receiving the new ceramic veneers. Depending on the indication and the type of veneers this may require the removal of a small amount of your tooth structures or an old restorative materials if present, normally around half a millimetre in thickness. In this appointment we also create your temporary veneers from the new prototype of your new smile. Thus we can evaluate the prototype in your mouth and test your phonetics bite and the way your smile looks. After your approval of all the elements of your new smile We’ll take an impression of it and send it to our trusted lab where a skilled ceramist will make your Ceramic veneers by hand under magnification of a microscope. Your completed restorations will be ready in approximately two to three weeks.
                 
                At this appointment, we will remove the temporaries, then place the porcelain veneers on your teeth with water or glycerin to check their fit and color. Once the Veneers meets your approval, he will clean your tooth to ensure a tight bond with the veneer. He’ll use bonding cement and a special light to permanently affix your veneers and revitalise your smile.
                 
                Bonding:
                 
                Definitely. Research and clinical studies indicate that whitening teeth under the supervision of a dentist is safe and effective. Many dentists consider teeth whitening the safest cosmetic dentistry procedure available. However, teeth bleaching is not recommended for children under 13 years of age or for pregnant or lactating women. And all the bleaching materials that we use hold an FDA approval, which is the highest standard for safety and quality.
                 
                Diagnosis and treatment planning:
                 
                Do veneers look like natural teeth?
                 
                Dr. Francis will examine and analyse your smile, face and bite. He’ll go over the details of the procedure and help you plan your new look. After which our experienced dental technicians will create a prototype of your new smile, which you will be able to see and discuss before any work is done on your teeth. Thus we eliminate any kind of unpredictable results and take in consideration every requirements you may have, and meeting your highest expectation.
                 
                Due to the circumstances of COVID-19, the clinic has new operating hours: <b>07:30am -  3:30pm from Monday to Friday, only with a prebook appointment.</b> 
                 
                First the implants are surgically placed into your jawbone to serve as tooth root substitutes and provide stable anchors for prosthetic teeth. Takes about three to six months for the implants to integrate with the jaw-bones.
                 
                Follow-Up Care and Maintenance:
                 
                For most patients, dental implant placement involves two procedures:
                 
                How durable are porcelain veneers?
                 
                In-Office Whitening
                 
                Is teeth whitening safe?
                 
                Once your jaw fuses with the dental implants, Dr. Francis will begin making your new teeth. He’ll take a dental impression and attach the abutment which is an attachment to the implants to secure your crowns or prosthetic, the abutment can be either prefabricated or a custom specially designed for your case either made from titanium or zirconium depends on each case.
                 
                Orthodontics is a part of dentistry concerning the proper development of  jaws,   arrangement of the teeth  and the removal the harmful habits such as mouth breathing , improper swallowing, tongue thrusting or thumb sucking. 
                 
                Our office offers Zoom! Whitening, the leading teeth whitening system, to lighten smiles up to eight shades in about an hour. You can sit back and relax while we apply special Zoom! Whitening gel to your teeth and activate it with a special light. The gel and light work in harmony to lift stains and discolourations and create dazzling smiles. This process is ideal for anyone who wants immediate results. We’ll also give you custom-made bleaching trays and professional-strength teeth whitening gel for at-home touchups to improve even more the In office bleaching results, and to maintain the brilliant white smile for a long time.
                 
                Porcelain veneers require three visits to design, create, and place. Here’s what you can expect:
                 
                Preparation:
                 
                The Porcelain Veneers Procedure
                 
                What can I expect from teeth whitening? 
                 
                What is a dental implant?
                 
                With proper care, porcelain veneers can brighten your smile for well over a decade. They’re highly resistant to stains from food, beverages, and tobacco, so your smile will stay beautiful. Dr. Francis will talk with you about your options for veneers. They come in many qualities and styles, including minimal or no preparation varieties that work well in some cases.
                 
                You’ll return for a follow-up visit after one to two weeks. In this visit we evaluate your oral hygiene and also a fine-tuning of your bite if needed. <br>
                Also if you grind or clench your teeth during your sleep we will make you a special invisible splint that will protect your veneers and relaxes your facial muscles during your sleep, to insure a longevity and comfort of your smile.
                 
        Browse <br class="visible-xs-block">
        <span class="text-normal">the space</span>
         53A Professor Kiril Popov Str., <span class="text-thin">
                        We<br>create<br>
                    </span>
                    <span class="text-light">smiles</span> Address After After her graduation she started two year training program in Centre for Smile Design. Beautifying Smiles with Professional Teeth Whitening. Before Booking an appointment is only done through phone Bulgarian Cases Challenges and Opportunities in Aesthetic Dentistry, NYU 2006 Change Close Contact Us Contacts Courses:  DR. Dano DR. Jenny DR. Magi DR. SELAR Danov Dental Dental Degree in 1988 from Dental School University of Baghdad. Dental Implants Dental Services Dental implants are a secure, natural-looking option to replace missing teeth. Direct and Indirect composite for anterior restoration 2004-2005, Italy Doctors Dr. Dano Danov Dr. Dano Danov Received dental education in the Faculty of Dentistry Sofia 1994. Dr. Jenny Kadreva Dr. Jenny Kadreva received her dental education in Faculty of Dental Medicine - Sofia in 2013. Dr. Magi Nashar Dr. Magi Nashar graduated Medical University of Sofia, Faculty of Dental Medicine in 2018. Currently a resident in a two year program in Centre for Smile Design. Dr. Selar Francis Endo with pro taper ADA 2003 English Francis From 1996 to 2010, attended training courses in the field of endodontics, aesthetic composite restorations and orthodontics in California, US. Three years postgrad program in Orthodontics "Full face philosophy", London UK, 2014.Currently student in MSc  program in Orthodontics- Danube University Austria. Implants It’s easier than ever to have a terrific smile. Every day, people just like you enhance their appearance and renew their confidence with professional teeth whitening. Our office offers a combination of an in-office and take-home bleaching to rejuvenate discoloured smiles. Kadreva Languages Mail Major training courses:  Many patients prefer implants to other prosthetics because they look and feel like natural teeth. Dental implants can renew your appearance, rejuvenate your oral health, and return your ability to eat, laugh, and smile with confidence.  MasArt of anterior composite restoration ADA 2004 Nashar Orthocaps Certification course, Berlin 2010 Orthodontics Phones Porcelain Porcelain Veneers Porcelain veneers are thin shells of ceramic that cover teeth to close gaps and eliminate imperfections. If you have stained, chipped, or misshapen teeth and want a completely custom solution to rejuvenate your smile, porcelain veneers may be right for you. Send Services Sofia 1700 Some of the Major attended courses:  Teeth Whitening Thanks The Clinic The Implant Process Veneers What are porcelain veneers? With more then 25 years of experience in cosmetic dentistry email message toothDental Project-Id-Version: django
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-01-25 23:04+0000
Last-Translator: Venelin Stoykov <vkstoykov@gmail.com>
Language-Team: Bulgarian (http://www.transifex.com/django/django/language/bg/)
Language: bg
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
<span class="text-extra-light">Зъбни</span> Импланти 
<span class="text-extra-light">Порцеланови</span> Фасети 
Избелване на <span class="text-extra-light">Зъби</span> 
Ортодонтия 
<span class="text-extra-light">Научи</span> повече 
<span class="text-thin">Д-р Дано</span> Данов 
<span class="text-thin">Д-р Джени</span> Къдрева 
<span class="text-thin">Д-р Маги</span> Нашар 
<span class="text-thin">Д-р Селар</span> Франсис 
                    <span class="text-thin">Избелване на</span><br>
                    <span class="text-light">зъби</span>
                     
Дентални<br><span class="text-normal">Услуги</span> 
НАБОРЪТ НИ ОТ УСЛУГИ ВКЛЮЧВА: 
Прекрасна усмивка! Избелването на зъбите може да премахне петна и потъмняване на зъбите, причинени от консумация на храни, напитки, тютюнопушене, и лекарства . Ние използваме най-модерните средства и технологии за избелване, чрез които безопасно и ефективно да ви осигурим прекрасна и блестяща усмивка. 
Зъбният имплант е титаниев винт, който наподобява корена на зъба. При липсващи зъби в костите на челюстите се поставят импланти, които да улеснят задържането и стабилността на цели или частични зъбни протези или мостови конструкции. Костта на челюстите се свързва с повърхноста на импланта, за да се създаде стабилна основа за поставянето на зъбна корона или протетична конструкция. Зъбните импланти възстановяват съзъбието, като променят визията на лицето и го подмладяват. Те предотвратяват и появата на костни дефекти, предизвикани от липсата на зъби.  
Абсолютно! Фасетите се изработват от фин дентален порцелан, който пречупва светлината като емайла на естествените зъби, поради което те имат вид и създават усещане за естествени зъби. На практика порцелановите фасети са неразличими от естествените зъби – никой няма да разбере за наличието им, но всеки ще забележи неустоимата ви усмивка. Тяхното сходство с естествените зъби не може да бъде постигнато с никакви други протетични средства.  
За поставянето на импланти е необходимо наличие на адекватна костна структура на челюстите. Д-р Франсис извършва подробен преглед  и използва <span class="text-semi-bold">последните дигитални технологии</span>, като <span class="text-semi-bold">конично-лъчевата компютърна томография (Cone Beam Computerized Tomography  – CBCT)</span>, която дава триизмерен образ на челюстите, ръководещ безопасното поставяне на имплантите. <br>Той ще ви разпита и за медицинската история, за да прецени дали сте подходящ за поставяне на импланти. Ако отговаряте на условията, д-р Франсис ще ви обясни процедурата и ще отговори на въпросите ви, които имат голямо значение за нас. Усилията на целия ни екип ще бъдат насочени към постигане на очакваните блестящи резултати от вашето лечение. 
Почти всеки, чиито зъби са с променен цвят или с петна, може да получи блестяща усмивка с професионално избелване на зъбите. Трябва да се има предвид, че при някои пациенти може да се наложи по-продължително избелване на зъбите, за да се постигне очакваният ефект. Ние ще ви прегледаме зъбите, за да преценим дали избелването е подходящ избор за вашата усмивка. 
Имам ли нужда от избелване на зъби? 
Подходящи ли са имплантите за мен? 
  
При това посещение д-р Франсис ще подготви зъбите за поставянето на новите порцеланови фасети. В зависимост от показанията и вида фасети може да се наложи отстраняването на малка част (обикновено около половин милиметър) от емайла на зъба или на стари материали за изграждане, ако има такива. При това посещение ще ви изработим и временни фасети от новия прототип на вашата усмивка. Така ще можем да тестваме прототипа в устата и да оценим говорната функция и естетическия вид на усмивката. След като получим вашето одобрение за всички показатели на новата ви усмивка, ще вземем отпечатък от зъбите и ще го изпратим до доверената ни зъботехническа лаборатория, където опитен зъботехник, специалист по керамични възстановявания, ще изработи ръчно вашите порцеланови фасети, използвайки микроскоп за увеличение. Вашите конструкции ще бъдат готови за около две до три седмици. 
 При това посещение ще свалим временните конструкции и ще поставим порцелановите фасети върху зъбите, използвайки вода или глицерин, за да проверим техния цвят, форма и прилягане към зъбите. След като одобрите конструкциите, ще почистим и изолираме зъбите, за да осигурим здравото им свързване с фасетите. За постоянното циментиране на фасетите д-р Франсис ще използва специален източник на светлина. 
Циментиране: 
Определено. Изследванията и клиничните изпитвания показват, че избелването на зъбите под контрола на дентален лекар е напълно безопасна и ефективна процедура. Според много дентални лекари избелването на зъбите е възможно най-безопасната козметична дентална процедура. Въпреки това избелването на зъбите не се препоръчва при деца под 13-годишна възраст и при бременни и кърмещи жени. Всички материали за избелване, които използваме в нашата клиника, са одобрени от американската Администрация по храни и лекарства (FDA), която поставя най-високите стандарти за безопасност и качество. 
Поставяне на диагноза и план на лечение: 
Изглеждат ли фасетите като естествени зъби? 
Д-р Франсис ще изследва и анализира вашите усмивка, лице и захапка. Той ще ви обясни подробно процедурата и ще ви помогне да планирате новия си външен вид. След това нашият опитен зъботехник ще изработи прототип на новата ви усмивка, след което прототипа се прехвърля върху вашите зъби. Вие ще имате възможност да оцените и обсъдите подробно този прототип, преди да пристъпим към подготовка на зъбите. Така ще елиминираме всички неочаквани и нежелани последствия, като се съобразим с изискванията ви, за да реализираме вашите очаквания. 
Предвид създалите се обстоятелства около Covid-19, клиниката е с ново работно време: <b>07:30ч. - 15:30ч. от понеделник до петък, само с предварително записан час.</b> 
Първо, чрез хирургична намеса малките винтове се поставят в костите на челюстите като заместители на зъбните корени, за да осигурят стабилни опори за протетично възстановяване на зъбите. Интегрирането на имплантите в челюста отнема около три до шест месеца. 
Последващи грижи и поддръжка 
При повечето пациенти поставянето на зъбни импланти включва две процедури: 
Колко дълготрайни са порцелановите фасети? 
Избелване на зъбите в клинични условия? 
Безопасно ли е избелването на зъбите? 
След като челюстите се интегрират с винтовете, имплантите се разкриват и на тях се поставят малки пръстени, подпомагащи оздравителния процес. След това д-р Франсис ще започне изработването на вашите нови протетични конструкции. Той ще вземе отпечатък от зъбите и ще прикрепи към импланта т. нар. абатмън, към който се свързва короната или протетичната конструкция. Абатмънът може да бъде фабричен или индивидуално изработен специално за вас от титан или цирконий според конкретния случай.  
Ортодонта е част от денталната медицина, касаеща правилното развитие на челюстите, подреждането на зъбите в зъбните редици и отстраняването и вредните навици като дишане през устата, неправилно гълтане, смучене на езика, смучене на пръст. 
В нашата клиника използваме водещата система за избелване Zoom! Whitening, за да постигнем само за един час ефект, до осем степени по-светъл от изходното състояние. В отпускаща и релаксираща за вас атмосфера ние ще нанесем специалния Zoom! Whitening гел върху вашите зъби и ще го активираме чрез специален светлинен източник. Гелът и светлината действат съвместно, за да отстранят петната и оцветяванията и да създадат блестяща усмивка. Тази процедура е подходяща за всеки, който желае да постигне незабавни резултати. Ние ще ви предоставим и индивидуални шини и професионален гел за избелване в домашни условия, за да поддържате добрите резултати от избелването в клинични условия и да запазите  своята блестяща бяла усмивка въпреки всичите оцветители в храните и напитките които консумирате ежедневно. 
Порцелановите фасети изискват три посещения за определяне на дизайна, за препариране на зъбите и за циментирането им. Ето какво можете да очаквате: 
Препарация на зъбите: 
Процедура за поставяне на порцеланови фасети 
Какво мога да очаквам от избелването на зъбите? 
Какво представлява зъбният имплант? 
С подходящи грижи порцелановите фасети могат да ви гарантират блестяща усмивка повече от десет години. Те са силно устойчиви на оцветявания от храни, напитки и тютюнопушене и запазват блясъка на усмивката. Д-р Франсис ще обсъди с вас подходящите варианти за фасети. Те могат да бъдат изработени от различни керамични матеряли, включително такива без препариране на зъбите приналичието на определени показания.  
След една до две седмици ще трябва да се явите на проследяващо посещение. При него ще оценим оралната ви хигиена и при необходимост ще нанесем леки корекции в захапката. <br>Ако стискате или скърцате със зъби, докато спите, ще ви изработим специална безцветна шина, която ще предпазва фасетите, както и вашите зъби, и ще съдейства за отпускане на лицевата мускулатура по време на сън, за да се гарантират дълготрайността и комфортът на вашата усмивка.<br>Желанието на всеки от нас за усмивка, която да оставя приятно впечатление у околните, е напълно естествено. Задължение на денталния лекар е да постигне баланс между естетическите изисквания на пациента и функционалните норми, като същевременно запази уникалните индивидуални характеристики на всяка усмивка. Нашата цел не е да променим тотално външния ви вид, а да го подобрим и балансираме. <br>В стремежа си да ви гарантираме възможно най-красивата усмивка ние не забравяме, че тя трябва да бъде и функционална и хармонична. Ние вярваме, че формата следва функцията и че усмивка, която е подходящо балансирана, ще притежава и изключителни естетични качества.  <br>Използваната от нас апаратура и технологии – последна дума на техниката, ще ви предложат несравнимото усещане за модерно и съвременно лечение. Най-новите и с ненадминати естетически качества материали, с които работим, гарантират дълготрайността и естествения вид на крайните резултати.  Нашата цел са максимално добри резултати, постигнати с минимално инвазивни процедури.<br> 
        Разгледай <br class="visible-xs-block">
        <span class="text-normal">Клиниката</span>
         ул. Професор Кирил Попов 53А <span class="text-thin">Ние<br>създаваме<br></span><span class="text-light">усмивки</span> Адрес След След дипломирането си започва след дипломно двогодишно обучение в Center for Smile Design. Красиви усмивки с професионално избелване на зъбите. Преди Записване на час става само по телефона български език Примери Предизвикателства и възможности на Естетичната Дентална Медицина NYU 2006 Промени Затвори Свържете се Контакти Курсове: Д-р Дано Д-р Джени Д-р Маги Д-р Селар Данов Дентални Д-р Франсис получава денталното си образувание в Денталния факултет на Университета в Багдат през 1988 г.  Зъбни импланти Дентални Услуги Зъбните импланти са безопасна и естествена на вид възможност за заместване на липсващи зъби. Дирeктни и индиректни композитни възстановявания  Италия 2004-2005г. Доктори Д-р Дано Данов Д-р Данов завършва дентално образование във факултета по дентална медицина гр. София 1994г. Д-р Джени Къдрева Д-р Джени Къдрева завършва Факултета по Дентална Медицина към Медицинския Университет - София, през 2013г. Д-р Маги Нашар Д-р Маги Нашар е завършила Факултета по Дентална Медицина към Медицинския Университет София през 2018 година. Д-р Селар Франсис Ендо с про-тейпър ADA 2003г. английски език Франсис От 1996-2010г. посещава курсове в областта на ендодонтията, естетичните композитни възстановявания и ортодонтията в Калифорния. През 2014г. Завършва 3г. Програма по ортодонтия „Full face philosophy“ Лондон ,Великобритания. В момента е студент „MSc in Orthodontics“ Danube University Austria.  импланти В днешни дни да се сдобиете с невероятна усмивка, е по-лесно от всякога. Всеки ден хора като вас подобряват външния си вид и повишават самочувствието си с помощта на професионално избелване на зъбите. Нашата клиника ви предлага комбинация от избелване в клинични и в домашни условия, за да вдъхнем нов живот на усмивките, загубили своята привлекателност.  Къдрева Езици Мейл По-важни курсове: Много пациенти предпочитат имплантите пред други алтернативни варианти, защото те изглеждат, функцйонират, и чустват като естествени зъби. Зъбните импланти могат да подмладят външния ви вид, да подобрят оралното ви здраве и да възвърнат възможността ви да се храните, смеете и усмихвате с увереност. Като опитен имплантолог, д-р Франсис възстановява усмивките на своите пациенти, като използва индивидуализиран подход в своята практика. Изкуството на фронталните възстановявания  ADA  2004г. Нашар Ортокапс сертификационен курс Берлин 2010г. Ортодонтия Телефони Порцеланови Порцеланови фасети Порцелановите фасети са тънки керамични пластини, които обвиват зъбите, за да затворят разстояния или да отстранят някои несъвършенства. Ако имате оцветени, начупени или деформирани зъби и желаете напълно индивидуализирано решение за подмладяване на вашата усмивка, порцелановите фасети могат да бъдат подходящ избор за вас. Изпрати Услуги София 1700 Някой от по-важните клинични курсове на обучение, които е завършил: Избелване на зъбите Благодаря Клиниката Процедура по поставяне на импланти фасети Какво представляват порцелановите фасети? С повече от 25 години опит в денталната медицина Eмейл Съобщениe Зъбни 