module.exports = function(gulp, paths, prefixer, rename, uglify, sass, browserSync, bsName, sourcemaps) {
    'use strict';
    var bs;

    if (browserSync.has(bsName)) {
        bs = browserSync.get(bsName);
    } else {
        bs = browserSync.create(bsName);
    }

    gulp.task('js-min', function() {
        return gulp.src(paths.js.watch)
            .pipe(uglify())
            // .pipe(rename({ extname: '.min.js' }))
            .pipe(bs.stream())
            .pipe(gulp.dest(paths.js.dest));
    });
};
