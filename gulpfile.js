'use strict';

var gulp = require('gulp');
var gulpDi = require('gulp-di')(gulp, {
    pattern: ['gulp-*', 'gulk.*', 'del', 'browser-sync'],
    rename: {
        'del': 'clean',
        'gulp-autoprefixer': 'prefixer',
    }
})
.provide({
    paths: {
        sass: {
            main: 'scss/main.scss',
            watch: ['scss/main.scss', 'scss/**/*.scss'],
            dest: 'smiledesign/static/css'
        },
        templates: {
            watch: ['templates/**/*', '**/templates/**/*']
        },
        js: {
            watch: 'smiledesign/static/js/*.js',
            dest: 'smiledesign/static/js/min'
        }
    },
    bsName: 'bs'
})
.tasks('./gulp')
.resolve();
