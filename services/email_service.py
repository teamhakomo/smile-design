from django.core.mail import EmailMessage, send_mail
from django.conf import settings
from django.template import loader

import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from sendgrid.helpers.mail import To


class EmailService(object):

    def send_template_email(self, to, subject, template, context, club=None, headers=None, unsubscribe_link=None):
        """
        This function prepares the information to fit correctly
        the template format and sends the email.
        Parameters:
        to(list) - recipients in list format
        subject - string
        template - the name of the email template
        context  - dictionary with the required values
        club - club instance, to generate his signature
        unsubscribe_link - will be done in the future

        """
        # template = loader.get_template('emails/' + template)

        # from_email = settings.SERVER_EMAIL
        # msg = EmailMessage(
        #     headers=headers,
        #     subject=subject,
        #     body=template.render(context),
        #     from_email=from_email,
        #     to=to
        # )
        # msg.content_subtype = "html"
        # msg.send()

        message = Mail(
        from_email=from_email,
        to_emails=to,
        subject=subject,
        html_content=template.render(context))
        try:
            sg = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY_SMILE_DESIGN'))
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
        except Exception as e:
            print(e.message)




    def send_email(self, name, customer_email, to, subject, message):
        # import pdb
        # pdb.set_trace()
        message = Mail(
        from_email='reception@smiledesign.bg',
        to_emails=To(to),
        subject=subject,
        html_content= 'From: {n} <br>Email: {e}<br>Message: {m}'.format(n=name,e=customer_email,m=message))
        try:
            api_key = os.environ.get('SENDGRID_API_KEY_SMILE_DESIGN')
            sg = SendGridAPIClient(api_key)
            response = sg.send(message)
            print(response.status_code)
            print(response.body)
            print(response.headers)
        except Exception as e:
            print('Error', e)
        # from_email = settings.SERVER_EMAIL
        # send_mail(
        #     subject, message, from_email, to, fail_silently=False
        # )
