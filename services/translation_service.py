from django.utils import translation


class TranslatedField(object):
    def __init__(self, en_field, bg_field):
        self.en_field = en_field
        self.bg_field = bg_field

    def __get__(self, instance, owner):
        if translation.get_language() == 'bg':
            bg_value = getattr(instance, self.bg_field)

            if not bg_value:
                return getattr(instance, self.en_field)
            else:
                return bg_value
        else:
            return getattr(instance, self.en_field)
