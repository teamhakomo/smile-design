# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views import generic
# Create your views here.


class CosmeticDentistry(generic.TemplateView):
    template_name = 'content/dental_services/cosmetic_dentistry.html'


class PorcelainVeneers(generic.TemplateView):
    template_name = 'content/dental_services/porcelain_veneers.html'


class DentalImplants(generic.TemplateView):
    template_name = 'content/dental_services/dental_implants.html'


class RestorativeDentistry(generic.TemplateView):
    template_name = 'content/dental_services/restorative_dentistry.html'


class TeethWhitening(generic.TemplateView):
    template_name = 'content/dental_services/teeth_whitening.html'


class Orthodontics(generic.TemplateView):
    template_name = 'content/dental_services/orthodontics.html'
