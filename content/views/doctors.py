# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views import generic
# Create your views here.


class DoctorFrancis(generic.TemplateView):
    template_name = 'content/doctors/doctor_francis.html'

class DoctorKadreva(generic.TemplateView):
    template_name = 'content/doctors/doctor_kadreva.html'

class DoctorDanov(generic.TemplateView):
    template_name = 'content/doctors/doctor_danov.html'

class DoctorNashar(generic.TemplateView):
    template_name = 'content/doctors/doctor_nashar.html'

class Doctor4(generic.TemplateView):
    template_name = 'content/doctors/doctor_4.html'
