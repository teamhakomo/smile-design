# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.core.urlresolvers import reverse_lazy
from django.views import generic

from services import EmailService

from content.models import Contact
from content.forms import ContactForm


class Contacts(generic.edit.CreateView):
    
    template_name = 'content/contacts.html'
    email_service = EmailService()

    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('thanks')

    def get_form_kwargs(self):
        kwargs = super(Contacts, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

    def form_valid(self, form):
        mail_context = form.cleaned_data
        subject = 'Website Contact Form: {}({})'.format(mail_context['name'], mail_context['email'])
        to = ['reception@smiledesign.bg']
        name=mail_context['name']
        customer_email=mail_context['email']
        # template = None

        # self.email_service.send_template_email(email, subject, template, mail_context)
        self.email_service.send_email(name, customer_email, to, subject, mail_context['message'])
        return super(Contacts, self).form_valid(form)


class ContactsThanks(generic.TemplateView):
    template_name = 'content/contacts_thanks.html'
