from __future__ import unicode_literals

from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse


class SiteSitemap(Sitemap):
    def items(self):
        return [
            'cosmetic_dentistry',
            'porcelain_veneers',
            'dental_implants',
            'restorative_dentistry',
            'teeth_whitening',
            'orthodontics',
            'doctors',
            'clinic_and_team',
            'contacts',
        ]

    def location(self, item):
        return reverse(item)
