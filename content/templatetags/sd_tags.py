import re

from django import template
from django.core.urlresolvers import reverse, NoReverseMatch

register = template.Library()


@register.simple_tag(takes_context=True)
def active_class_for_url(context, url):
    try:
        pattern = '^%s$' % reverse(url)
    except NoReverseMatch:
        pattern = url

    path = context['request'].path
    return 'active' if re.search(pattern, path) else ''


@register.simple_tag(takes_context=True)
def get_site_root(context):
    # This returns a core.Page. The main menu needs to have the site.root_page
    # defined else will return an object attribute error ('str' object has no
    # attribute 'get_children')
    return context['request'].site.root_page


@register.simple_tag(takes_context=True)
def subpages_for_page(context, page):
    return page.get_children().live()


@register.simple_tag(takes_context=True)
def active_class_for_page(context, page, current_page):
    if current_page:
        return 'active' if current_page.url == page.url else ''

    return ''
