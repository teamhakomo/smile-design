from django import forms

from antispam import akismet
from . import models


class ContactForm(forms.ModelForm):
    class Meta:
        model = models.Contact
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(ContactForm, self).__init__(*args, **kwargs)

    def clean(self):
        if akismet.check(
            request=akismet.Request.from_django_request(self.request) if self.request else None,
            comment=akismet.Comment(
                content=self.cleaned_data['message'],
                type='message',
                author=akismet.Author(
                    name=self.cleaned_data['name'],
                    email=self.cleaned_data['email']
                )
            )
        ):
            raise forms.ValidationError('Spam detected', code='spam-protection')

        super(ContactForm, self).clean()
