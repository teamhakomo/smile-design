# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from .models import Contact


class ContactAdmin(admin.ModelAdmin):
    '''
        Admin View for Contact
    '''
    list_display = ['name', 'email']

admin.site.register(Contact, ContactAdmin)
