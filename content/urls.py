# django imports
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings

# internal imports
from . import views


urlpatterns = [
    url(r'^dental_services/cosmetic_dentistry/$',
        views.CosmeticDentistry.as_view(),
        name='cosmetic_dentistry'),

    url(r'^dental_services/porcelain_veneers/$',
        views.PorcelainVeneers.as_view(),
        name='porcelain_veneers'),

    url(r'^dental_services/dental_implants/$',
        views.DentalImplants.as_view(),
        name='dental_implants'),

    url(r'^dental_services/restorative_dentistry/$',
        views.RestorativeDentistry.as_view(),
        name='restorative_dentistry'),

    url(r'^dental_services/teeth_whitening/$',
        views.TeethWhitening.as_view(),
        name='teeth_whitening'),

    url(r'^dental_services/orthodontics/$',
        views.Orthodontics.as_view(),
        name='orthodontics'),

    url(r'^doctors/doctor_francis/$',
        views.DoctorFrancis.as_view(),
        name='doctor_francis'),

    url(r'^doctors/doctor_kadreva/$',
        views.DoctorKadreva.as_view(),
        name='doctor_kadreva'),

    url(r'^doctors/doctor_danov/$',
        views.DoctorDanov.as_view(),
        name='doctor_danov'),

    url(r'^doctors/doctor_nashar/$',
        views.DoctorNashar.as_view(),
        name='doctor_nashar'),

    url(r'^doctors/doctor4/$',
        views.Doctor4.as_view(),
        name='doctor_4'),

    url(r'^clinic_and_team/$',
        views.ClinicAndTeam.as_view(),
        name='clinic_and_team'),

    url(r'^download_forms/$',
        views.DownloadForms.as_view(),
        name='download_forms'),

    url(r'^contacts/$',
        views.Contacts.as_view(),
        name='contacts'),

    url(r'^contacts/thanks/$',
        views.ContactsThanks.as_view(),
        name='thanks'),
]
